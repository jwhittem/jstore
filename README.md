# jstore

Simple in-memory JSON store, using Go's new sync.map 

```
Usage of ./jstore:
  -port string
        port to listen on. (default ":8080")
```