package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"sync"
)

var (
	jstore sync.Map
	port   string
)

type message struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

func main() {
	flag.StringVar(&port, "port", ":8080", "port to listen on.")
	flag.Parse()

	http.HandleFunc("/get", func(w http.ResponseWriter, r *http.Request) {
		get(w, r)
	})
	http.HandleFunc("/set", func(w http.ResponseWriter, r *http.Request) {
		set(w, r)
	})

	http.ListenAndServe(port, nil)
}

func check(e error) {
	if e != nil {
		fmt.Println(e)
	}
}

func get(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var req message
	var resp message

	err := json.NewDecoder(r.Body).Decode(&req)
	check(err)
	resp.Key = req.Key

	if v, ok := jstore.Load(req.Key); ok {
		resp.Value = fmt.Sprint(v)
	} else {
		resp.Value = "null"
	}
	err = json.NewEncoder(w).Encode(resp)
	check(err)
}

func set(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	var req message
	err := json.NewDecoder(r.Body).Decode(&req)
	check(err)

	jstore.Store(req.Key, req.Value)
}
